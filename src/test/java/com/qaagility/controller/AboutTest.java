package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;


public class AboutTest {

	@Test
	public void testDesc() throws Exception {
        	final About aboot = new About();
		final String result = aboot.desc();
		assertEquals("Description","This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!", result);
	}
}
