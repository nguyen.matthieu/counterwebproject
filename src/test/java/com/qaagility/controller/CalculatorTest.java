package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;


public class CalculatorTest {

	@Test
	public void testAdd() throws Exception {
		final int result = new Calculator().add();
		assertEquals("Add", 9 , result);
	}
}
