package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;


public class CounterTest {

	@Test
	public void testDivideByZero() throws Exception {
		final int result = new Counter().divide(1,0);
		assertEquals("Divide By Zero", Integer.MAX_VALUE , result);
	}
        @Test
        public void testDivision() throws Exception {
                final int result = new Counter().divide(10,2);
                assertEquals("Divide By non-zero value", 5 , result);
        }



}
